<%-- 
    Document   : login
    Created on : 12/10/2014, 03:43:15 PM
    Author     : fer
--%>

<%@ page contentType="text/html; charset=iso-8859-1" language="java" %>
<%
String error=request.getParameter("error");
if(error==null || error=="null"){
 error="";
}
%>
<html>
<head>
<title>Login JSP</title>
<script>
    function trim(s) 
    {
        return s.replace( /^s*/, "" ).replace( /s*$/, "" );
    }

    function validate()
    {
        if(trim(document.frmLogin.name.value)=="")
        {
          alert("ingresa el usuario");
          document.frmLogin.name.focus();
          return false;
        }
        else if(trim(document.frmLogin.password.value)=="")
        {
          alert("la contraseņa es obligatoria");
          document.frmLogin.sPwd.focus();
          return false;
        }
    }
     function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
 
return true;
}
</script>
</head>

<body>
<div><%=error%></div>
<form name="frmLogin" onSubmit="return validate();" action="login.jsp" method="post">
Usuario <input type="text" name="name" /><br />
Contraseņa <input  onkeypress="return isNumberKey(event)" type="password" name="password" /><br />
<input type="submit" name="submit" value="Entrar" />
</form>
</body>
</html>

