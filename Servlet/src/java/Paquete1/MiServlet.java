/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Paquete1;

import java.io.*;
import java.util.logging.*;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.sql.*;
import javax.servlet.ServletContext;


public class MiServlet extends HttpServlet {

    private ConfiguraDB db = null;
    String drivers, urlDB, usrDB, passDB;

    public void init(ServletConfig config) throws ServletException {
        try {
            db = new ConfiguraDB(config);
            
        } catch (Exception e) {
        }
    }

/////OJO SE ALTERA PARTE DEL CODIGO DEL METODO PROCCESSREQUEST
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Cueva de Cuates</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1 align=center> <br> <b> Cueva de cuates </b> <br> </h1>  "
                    + " <hr><br> <h3 align=center> <br> Bienvenida(o)  " + request.getParameter("user") + "</h3>");
            out.println("</body>");
            out.println("</html>");

        } finally {
            out.close();
        }
    }

/////OJO SE ALTERA PARTE DEL CODIGO DEL METODO doPOST

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("user");//nombres del formulario
        String passwd = request.getParameter("pwd");
        if (Entrar(nombre, passwd)) {
            processRequest(request, response);
        } else {
            aLosPerrosForm(response);
        }
    }

    private boolean Entrar(String nombre, String passwd) {
        boolean salida = false;
        try {
            System.out.println("Creando conexion");
            Connection con = db.CrearConexion();
            System.out.println("Creando statement");
            Statement s = con.createStatement();
            String sql = "SELECT nombre FROM cuates"  // OJO AQUI PON EL NOMBRE DE TU TABLA
                    + " WHERE nombre='" + nombre + "'"
                    + " AND password='" + passwd + "'";
            ResultSet rs = null;
            System.out.println("Ejecutar SQL: " + sql);
            if (s.execute(sql)) {
                rs = s.getResultSet();
                if (rs.next()) {
                    salida = true;
                    return salida;
                }
               // Existe al menos un registro
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(MiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salida;
    }

    private void aLosPerrosForm(HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter salida = response.getWriter();
        salida.println("<HTML><HEAD><TITLE>Entrada</TITLE></HEAD><BODY>");
        salida.println("<br><br> <b> <h1 align=center> &#161ALERTA DE INTRUSO&#33</h1>"
                + " <hr> <br> <h2 align=center >&#161LLAMEN A LOS PERROS&#33<br><br>");
        salida.println("</body>");

    }


    class ConfiguraDB {

        public ConfiguraDB(ServletConfig config) throws IOException {
try{
java.lang.Class.forName(config.getInitParameter("jdbc.drivers"));//PARA CORREGIR ERROR 500

}catch(ClassNotFoundException ex){
//Logger.getLogger()

}
            drivers = config.getInitParameter("jdbc.drivers");
            urlDB = config.getInitParameter("jdbc.url");
            usrDB = config.getInitParameter("jdbc.username");
            passDB = config.getInitParameter("jdbc.password");
        }

        public Connection CrearConexion() {
        Connection conexion = null;
        try {
            System.out.println("Creando conexion a \"" + urlDB + "\"....");
            conexion =
                    DriverManager.getConnection(urlDB, usrDB, passDB);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return conexion;
    }

    }
}